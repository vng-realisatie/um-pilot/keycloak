# UM-Pilot

UM bestaat uit twee software componenten.
1. UM
2. Externe vertaalapplicatie

Deze software is nog in ontwikkeling en is bedoeld als beproeving van de architectuurprincipes en omvat de MVP functionaliteit van UM.
Deze functionaliteit is voldoende om de VUM pilots te kunnen ondersteunen.

De code is beschikbaargesteld onder de EUPL 1.2 voorwaarden
https://www.eupl.eu/1.2/nl/

## Keycloak

### Configuratie realm in een docker omgeving
Hieronder enkele beknopte opmerkingen m.b.t. tot de realm van Keycloak, wanneer Keycloak draait als een docker container.

- De realms in de realm-config map worden ingeladen bij het opstarten van Keycloak middels de docker compose file. De filenaam dient dan wel op "realm.json" te eindigen! Dit laatste is een eigenschap van Keycloak zelf.


- De file realm-export.json in de map conf, wordt gebruikt in de huidige versie van de docker compose file in het um compose project. Het is de bedoeling dat dit veranderd en de file niet meer uit deze directory opgepakt wordt.

- Voorbeeld commando om een export van alle realms met gebruikers naar een locatie in de docker container (De export functie in GUI exporteert geen gebruikers): 
```
docker exec -it keycloak /opt/jboss/keycloak/bin/standalone.sh -Djboss.socket.binding.port-offset=100 -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=dir -Dkeycloak.migration.usersExportStrategy=REALM_FILE -Dkeycloak.migration.dir=/opt/jboss/keycloak/themes/customlogin/
```
- Eenvoudige manier om een realm te dupliceren. Maak een export (json file). Verwijder alle regels met de key "ID". Verander alle regels met de naam van de Realm. Importeer deze file door deze in de bovenstaande map te zetten (en start Keycloak opnieuw, of importeer via de GUI admin console.

